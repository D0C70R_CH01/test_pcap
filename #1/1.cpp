/*code by DoctorChoi (https://gitlab.com/DoctorChoi) */

// 그저 테스트를 할 뿐임

#include "iostream"
#include "iomanip"
#include "pcap.h"
//import 끗

#pragma comment(lib, "wpcap.lib")
//라이브러리 링크 끗

#define null NULL
#define true 1
#define false 0

void packet_handler(unsigned char * param, const struct pcap_pkthdr * header, const unsigned char *pkt_data)
{
    for(int i = 1; i < header->caplen + 1; i++)
    {
        std::cout << std::setw(2) << std::hex << pkt_data[i - 1] << " ";
        if((i % 16) == 0) std::cout << std::endl;
    }

    std::cout << std::endl << std::endl;
}
//패킷 들어올때 호출되는 함수 끗

int main(int argc, char * argv[])
{
    pcap_if_t * devList;
    pcap_if_t * dev;
    pcap_t * adhandle;
    char err[PCAP_ERRBUF_SIZE];
    int mode = 0;
    int i = 0;
    int n = 0;
    //변수선언 끗

    if(pcap_findalldevs(&devList, err) == -1)
    {
        std::fprintf(stderr, "pcap_findalldevs : %s\n", err);
        std::exit(1);
    }
    //디바이스 찾는 함수에서 오류나면 종료
    
    for(dev = devList; dev; dev = dev->next)
    {
        std::cout << ++i << ". " << dev->name;

        if(dev->description) std::cout << " (" << dev->description << ")" << std::endl;
        else std::cout << " (설명 없음)" << std::endl;
    }
    //디바이스 목록을 띄워줌

    if(i == 0)
    {
        std::fprintf(stderr, "\n 네트워크 디바이스를 찾을수 없음\n");
        std::exit(1);
    }
    //디바이스가 하나도 없으면 종료

    std::cout << "디바이스 번호 입력 1~" << i << " : ";
    std::cin >> n;
    //캡처할 디바이스를 입력받음

    if(n < 1 || n > i)
    {
        std::cout << "정상적인 값을 넣어주세요\n";
        pcap_freealldevs(devList);
        exit(-1);
    }
    //이상한거 들어오면 종료

    for(dev = devList, i = 0; i < n - 1; dev = dev->next, i++);
    //디바이스 선택

    if((adhandle = pcap_open_live(dev->name, 65536, 1, 1000, err)) == null)
    {
        fprintf(stderr, "\n네트워크 디바이스가 PCAP를 지원하지 않음\n");
        pcap_freealldevs(devList);
        exit(-1);
    }
    //지원 안하면 종료

    std::cout << "캡쳐중...:" << std::endl;
    pcap_freealldevs(devList);
    //필요없는 디바이스 정보 제거

    pcap_loop(adhandle, 0, packet_handler, null);
    //캡쳐

    pcap_close(adhandle);
    //종료

    return 0;
}
//메인함수 끗
